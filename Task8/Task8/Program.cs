﻿using System;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get height from the user
            Console.WriteLine("Please enter your height (in meters. Use dot for decimal point):");
            double height = double.Parse(Console.ReadLine());

            // Get weight from the user
            Console.WriteLine("Please enter your weight (in kg. Use dot for decimal point):");
            double weight = double.Parse(Console.ReadLine());

            // Calculate their BMI
            double bmi = CalculateBMI(weight, height);

            // Output bmi to user
            Console.WriteLine("Your bmi is: " + bmi);

            // Output weight category to user
            Console.WriteLine(GetWeightCategory(bmi));
        }

        public static double CalculateBMI(double weight, double height)
        {
            return weight / Math.Pow(height, 2); ;
        }

        public static string GetWeightCategory(double bmi)
        {

            string category;

            if (bmi < 18.5)
            {
                category = "You fall into the underweight category";
            }
            else if (bmi < 24.9)
            {
                category = "You fall into the normal weight category";
            }
            else if (bmi < 29.9)
            {
               category = "You fall into the overweight category";
            }
            else
            {
                category = "You fall into the obese category";
            }

            return category;
        }
    }
}
