﻿using System;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get height from the user
            Console.WriteLine("Please enter your height (in meters. Use dot for decimal point):");
            double height = double.Parse(Console.ReadLine());

            // Get weight from the user
            Console.WriteLine("Please enter your weight (in kg. Use dot for decimal point):");
            double weight = double.Parse(Console.ReadLine());

            // Calculate their BMI
            double bmi = weight / Math.Pow(height, 2);

            // Output bmi to user
            Console.WriteLine("Your bmi is: " + bmi);

            // Output weight category to user
            if(bmi < 18.5)
            {
                Console.WriteLine("You fall into the underweight category");
            }
            else if(bmi < 24.9)
            {
                Console.WriteLine("You fall into the normal weight category");
            }
            else if(bmi < 29.9)
            {
                Console.WriteLine("You fall into the overweight category");
            }
            else
            {
                Console.WriteLine("You fall into the obese category");
            }
        }
    }
}
